import React, { Component } from 'react';
import axios from 'axios';
import Navbar from './components/common/navbar';
import Messages from './components/common/messages';
import PlannerView from './components/plannerView/plannerView';
import CalendarView from './components/calendarView/calendarView';

class App extends Component {

  state = {
    errors: [],
    view: 0,
    redirect: false,
    user: null,
    version: ''
  }

  componentDidMount() {
    axios.get('/api/user')
        .then(res => {
            if(typeof res.data.username != 'undefined'){
               this.setState({
                   user: res.data,
               });
            }
            else if(res.data === false){
                this.redirect();
            }
            else{
                this.throwError('Error: Unable to obtain user information. ERR: USER_INFO');
            }
        })
        .catch(err => this.throwError('Error: Unable to connect to server. ERR: USER_INFO'));
    
    axios.get('/api/version')
      .then(res => {
        if(res.data === false){
          this.redirect();
        }
        else if(res.data === 'err'){
          this.throwError('Error: Unable to obtain version information. ERR: VERSION_INFO');
        }
        else{
          this.setState({
            version: res.data
        });
        }
    })
    .catch(err => this.throwError('Error: Unable to obtain version information. ERR: VERSION_INFO'));
}

  redirect = () => {
    this.setState({redirect: true});
  }

  throwError = (msg) => {
    let errors = [...this.state.errors];
    errors.push({msg: msg, id: Date.now()});
    this.setState({errors: errors});
  }

  cancelError = (id) => {
    let errors = [...this.state.errors];
    errors = errors.filter(error => error.id !== id);
    this.setState({errors: errors});
  }

  setView = (view) => {
    this.setState({view: view});
  }

  getView = () => {
    if(this.state.view === 0){
      return <PlannerView throwError={this.throwError} redirect={this.redirect} use24Hour={this.state.user.use24Hour} />;
    }
    else{
      return <CalendarView throwError={this.throwError} redirect={this.redirect} />;
    }
  }

  render(){
    if(this.state.redirect){
      if(window.location.port === "3000"){
        window.location.href = "http://localhost:5000/login";
      }
      else{
        window.location.href = '/login';
      }
    }
    else if(this.state.user !== null){
      return (
        <div>
          <Navbar throwError={this.throwError} setView={this.setView} view={this.state.view} redirect={this.redirect} user={this.state.user} version={this.state.version} />
          <Messages errors={this.state.errors} cancelError={this.cancelError} />
          {this.getView()}
        </div>
      );
    }
    else return null;
  }
}

export default App;
