import React, { Component } from 'react';

class Navbar extends Component {
    checkAdmin() {
        if(this.props.user.admin){
            return (
                <React.Fragment>
                    <a className="dropdown-item" href="/users">Users</a>
                    <div className="dropdown-divider"></div>                    
                </React.Fragment>
            );
        }
        else{
            return;
        }
    }

    render() { 
        return ( 
            <nav className="navbar-expand navbar-dark bg-dark">
                <div className="collapse navbar-collapse">
                    <a className="navbar-brand ml-2" href="/">
                        <img src="/assets/logo.png" width="30" height="30" className="d-inline-block align-top mr-1" alt="" />
                         Taskmaster
                    </a>
                    <span className="navbar-text">{this.props.version}</span>
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item dropdown">
                            <button className="btn nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{this.props.user.username}</button>
                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <div className="dropdown-item">
                                    <button className="btn btn-link" onClick={() => this.props.setView(0)}>Planner</button>
                                    <button className="btn btn-link" onClick={() => this.props.setView(1)}>Calendar</button>
                                </div>
                                <div className="dropdown-divider"></div>
                                {this.checkAdmin()}
                                <a className="dropdown-item" href="/account">Account</a>
                                <a className="dropdown-item" href="/logout">Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
         );
    }
}
 
export default Navbar;