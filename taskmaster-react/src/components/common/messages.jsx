import React, { Component } from 'react';
import Message from './message';

class Messages extends Component {
    render() { 
        return ( 
            <div>
                {this.props.errors.map(error => <Message key={error.id} id={error.id} msg={error.msg} cancelError={this.props.cancelError} />)}
            </div>
        );
    }
}
 
export default Messages;