import React, { Component } from 'react';

class Message extends Component {

    render() { 
        return (
            <div className="alert alert-danger m-1" role="alert">
                <button className="btn btn-danger close" onClick={() => this.props.cancelError(this.props.id)}>
                    <span className="p-2 m-1">×</span>
                </button>
                <p>{this.props.msg}</p>
            </div> 
        );
    }
}
 
export default Message;