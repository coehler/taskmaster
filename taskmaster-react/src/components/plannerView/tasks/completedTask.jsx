import React, { Component } from 'react';

class CompletedTask extends Component {
    
    render() { 
        return (
            <div className="container-fluid bg-light rounded p-1 pt-2 pb-3 mt-1 mb-1 planTask">
                <button className="btn btn-sm d-inline btn-warning float-right" onClick={() => this.props.undoTask(this.props.id)}>Undo</button>
                <p className="d-inline text-dark">{this.props.task}</p>
            </div>
        );
    }
}
 
export default CompletedTask;