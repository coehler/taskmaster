import React, { Component } from 'react';
import GroupSetter from './groupSetter';

import TimeSetter from './timeSetter/timeSetter';

class Task extends Component {
    state = { 
        state: 0,
        newTask: '',
        time: {
            startTime: {
                h: 0,
                m: 0
            },
            endTime: {
                h: 0,
                m: 0
            }
        }
    }

    timeFunctions = {
        changeStartHour: (amount) => {
            let time = {...this.state.time};
            time.startTime.h += amount;
            if(time.startTime.h > 23){
                time.startTime.h = 0;
            }
            if(time.startTime.h < 0){
                time.startTime.h = 23;
            }
            this.setState({time: time});
        },
        changeStartMinute: (amount) => {
            let time = {...this.state.time};
            time.startTime.m += amount;
            if(time.startTime.m > 59){
                time.startTime.m = 0;
            }
            if(time.startTime.m < 0){
                time.startTime.m = 59;
            }
            this.setState({time: time});
        },
        changeEndHour: (amount) => {
            let time = {...this.state.time};
            time.endTime.h += amount;
            if(time.endTime.h > 23){
                time.endTime.h = 0;
            }
            if(time.endTime.h < 0){
                time.endTime.h = 23;
            }
            this.setState({time: time});
        },
        changeEndMinute: (amount) => {
            let time = {...this.state.time};
            time.endTime.m += amount;
            if(time.endTime.m > 59){
                time.endTime.m = 0;
            }
            if(time.endTime.m < 0){
                time.endTime.m = 59;
            }
            this.setState({time: time});
        },
        resetTime: () => {
            let time = {
                startTime: {
                    h: 0,
                    m: 0
                },
                endTime: {
                    h: 0,
                    m: 0
                }
            }
            this.setState({time: time});
        },
        setStartTime: (h, m) => {
            let time = {...this.state.time};
            time.startTime.m = m;
            time.startTime.h = h;
            this.setState({time: time});
        },
        setEndTime: (h, m) => {
            let time = {...this.state.time};
            time.endTime.m = m;
            time.endTime.h = h;
            this.setState({time: time});
        }
    }

    componentDidUpdate(){
        if(this.state.state === 1){
            this.taskInput.focus();
        }
    }

    changeState = (n) => {
        this.timeFunctions.resetTime();
        this.setState({state: n});
    }

    editTask = (e) => {
        if(e.key === 'Enter'){
            this.changeState(0);
            this.props.updateTask(this.props.id, this.state.newTask);
        }
    }

    getGroupButton = () => {
        if(this.props.grouped){
            return <button className="btn btn-sm d-inline btn-light float-right mr-2" onClick={() => this.props.ungroup(this.props.id)}>Ungroup</button>;
        }
        else{
            return <button className="btn btn-sm d-inline btn-light float-right mr-2" onClick={() => this.changeState(3)}>Group</button>;
        }
    }

    group = (taskID, groupID) => {
        this.changeState(0);
        this.props.group(taskID, groupID);
    }

    getElements = () => {
        if(this.state.state === 1){
            return(
                //Edit
                <React.Fragment>
                    <button className="btn btn-sm d-inline btn-danger float-right" onClick={() => this.changeState(0)}>Cancel</button>
                    <button className="btn btn-sm d-inline btn-warning float-right mr-2" onClick={() => this.props.deleteTask(this.props.id)}>Delete</button>
                    <button className="btn btn-sm d-inline btn-success float-right mr-2" onClick={() => this.props.completeTask(this.props.id)}>Done</button>
                    <input
                        type="text"
                        className="form-control mr-1 col-lg-8 d-inline"
                        id="title"
                        onChange={event => {this.setState({newTask: event.target.value})}}
                        onKeyPress={this.editTask}
                        ref={(input) => { this.taskInput = input; }}
                        defaultValue={this.props.task}
                    />
                </React.Fragment>
            );
        }
        else if(this.state.state === 2){
            return(
                //Plan
                <React.Fragment>
                    <button className="btn btn-sm d-inline btn-danger float-right" onClick={() => this.changeState(0)}>Cancel</button>
                    <button className="btn btn-sm d-inline btn-success float-right mr-2" onClick={() => this.props.planTask(this.props.id, this.state.time)}>Plan</button>
                    <p className="d-inline">{this.props.task}</p>
                    <TimeSetter time={this.state.time} timeFunctions={this.timeFunctions} use24Hour={this.props.use24Hour} />
                </React.Fragment>
            );
        }
        else if(this.state.state === 3){
            return(
                //Group
                <React.Fragment>
                    <button className="btn btn-sm d-inline btn-danger float-right" onClick={() => this.changeState(0)}>Cancel</button>
                    <p className="d-inline">{this.props.task}</p>
                    <GroupSetter groups={this.props.groups} group={this.group} taskID={this.props.id} />
                </React.Fragment>
            );
        }
        else{
            return (
                <React.Fragment>
                    <button className="btn btn-sm d-inline btn-warning float-right" onClick={() => this.changeState(1)}>Edit</button>
                    <button className="btn btn-sm d-inline btn-info float-right mr-2" onClick={() => this.changeState(2)}>Plan</button>
                    {this.getGroupButton()}
                    <p className="d-inline">{this.props.task}</p>
                </React.Fragment>
            );
        }
    }

    render() { 
        return (
            <div className={"container-fluid bg-dark rounded p-1 pt-2 pb-3 mt-1 mb-1 overflow-hidden"}>
                {this.getElements()}
            </div>
        );
    }
}
 
export default Task;