import React, { Component } from 'react';
import timeConverter from '../../../scripts/timeConverter';

import Task from './task';
import PlannedTask from './plannedTask';
import CompletedTask from './completedTask';
import Group from './group';

class Tasks extends Component {
    state = {
        create: false,
        newTask: '',
        createGroup: false,
        newGroup: ''
    }

    componentDidUpdate(){
        if(this.state.create){
            this.taskInput.focus();
        }
        else if(this.state.createGroup){
            this.groupInput.focus();
        }
    }

    newTask = () => {
        this.setState({
            create: true,
            newTask: ''
        });
    }

    newGroup = () => {
        this.setState({
            createGroup: true,
            newGroup: ''
        });
    }

    cancelOptions = () => {
        this.setState({create: false, createGroup: false});
    }

    addTask = (e) => {
        if(e.key === 'Enter'){
            this.cancelOptions();
            this.props.addTask(this.state.newTask);
        }
    }

    addGroup = (e) => {
        if(e.key === 'Enter'){
            this.cancelOptions();
            this.props.addGroup(this.state.newGroup);
        }
    }

    getNav = () => {
        if(this.state.create){
            return (
                <React.Fragment>
                    <button className="btn btn-danger btn-sm m-2 d-inline float-right m-0" onClick={() => this.cancelOptions()}>Cancel</button>
                    <input
                        type="text"
                        className="form-control m-1"
                        onChange={event => {this.setState({newTask: event.target.value})}}
                        onKeyPress={this.addTask}
                        ref={(input) => { this.taskInput = input; }}
                    />
                </React.Fragment>
            );
        }
        else if(this.state.createGroup){
            return (
                <React.Fragment>
                    <button className="btn btn-danger btn-sm m-2 d-inline float-right m-0" onClick={() => this.cancelOptions()}>Cancel</button>
                    <input
                        type="text"
                        className="form-control m-1"
                        onChange={event => {this.setState({newGroup: event.target.value})}}
                        onKeyPress={this.addGroup}
                        ref={(input) => { this.groupInput = input; }}
                    />
                </React.Fragment>
            );
        }
        else{
            return (
                <div>
                    <button className="btn btn-success btn-sm m-2 d-inline float-right" onClick={() => this.newTask()}>New Task</button>
                    <button className="btn btn-info btn-sm m-2 d-inline float-right" onClick={() => this.newGroup()}>New Group</button>
                </div>
            );
        }
    }

    renderPlanned = () => {
        if(this.props.plannedTasks.length > 0){
            return (
                <React.Fragment>
                    <hr className="hr-text" data-content="PLANNED TASKS" />
                    {this.props.plannedTasks.map(plan => <PlannedTask key={plan._id} id={plan._id} task={plan.task} time={timeConverter.convertTimeToDisplay(this.props.use24Hour, plan.time.startTime.h, plan.time.startTime.m)+'  to  '+timeConverter.convertTimeToDisplay(this.props.use24Hour, plan.time.endTime.h, plan.time.endTime.m)} timeObj={plan.time} unplanTask={this.props.unplanTask} replanTask={this.props.replanTask} use24Hour={this.props.use24Hour} />)}
                </React.Fragment>
            );
        }
        else {
            return null;
        }
    }

    renderDone = () => {
        if(this.props.completedTasks.length > 0){
            return (
                <React.Fragment>
                    <hr className="hr-text" data-content="COMPLETED TASKS" />
                    {this.props.completedTasks.map(plan => <CompletedTask key={plan._id} id={plan._id} task={plan.task} undoTask={this.props.undoTask} />)}
                </React.Fragment>
            );
        }
        else {
            return null;
        }
    }

    renderTasks = () => {
        let groups = [];
        let headerPlaced = false;
        this.props.tasks.forEach(group => {
            if(group.groupName !== null){
                if(!headerPlaced){
                    groups.push(<hr key={0} className="hr-text" data-content="GROUPS" />);
                    headerPlaced = true;
                }
                groups.push(<Group key={group._id} id={group._id} groupName={group.groupName} tasks={group.tasks} updateGroup={this.props.updateGroup} deleteGroup={this.props.deleteGroup} updateTask={this.props.updateTask} deleteTask={this.props.deleteTask} planTask={this.props.planTask} completeTask={this.props.completeTask} ungroup={this.props.ungroup} addTaskAndGroup={this.props.addTaskAndGroup} />)
            }
            else{
                group.tasks.map(task => groups.push(<Task key={task._id} id={task._id} task={task.task} grouped={false} updateTask={this.props.updateTask} deleteTask={this.props.deleteTask} planTask={this.props.planTask} completeTask={this.props.completeTask} groups={this.props.groups} group={this.props.group} use24Hour={this.props.use24Hour} />));
            }
        });
        return groups;
    }

    render() {
        return (
            <div className="container-fluid col mr-1 right-border">
                 <nav className="navbar navbar-dark bg-dark sticky-top p-0">
                    <h4 className="m-0 p-0 d-inline ml-2">Tasks</h4>
                    {this.getNav()}
                </nav>
                <div className="scrollWindows">
                    {this.renderTasks()}
                    {this.renderPlanned()}
                    {this.renderDone()}
                </div>
            </div>
        );
    }
}
 
export default Tasks;