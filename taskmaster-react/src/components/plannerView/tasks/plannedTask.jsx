import React, { Component } from 'react';

import TimeSetter from './timeSetter/timeSetter';

class PlannedTask extends Component {

    state = {
        replan: false,
        replanTime: {}
    }

    componentDidMount(){
        const replanTime = {
            startTime: {
                h: this.props.timeObj.startTime.h,
                m: this.props.timeObj.startTime.m
            },
            endTime: {
                h: this.props.timeObj.endTime.h,
                m: this.props.timeObj.endTime.m
            }
        }
        this.setState({replanTime: replanTime});
    }

    timeFunctions = {
        changeStartHour: (amount) => {
            let replanTime = {...this.state.replanTime};
            replanTime.startTime.h += amount;
            if(replanTime.startTime.h > 23){
                replanTime.startTime.h = 0;
            }
            if(replanTime.startTime.h < 0){
                replanTime.startTime.h = 23;
            }
            this.setState({replanTime: replanTime});
        },
        changeStartMinute: (amount) => {
            let replanTime = {...this.state.replanTime};
            replanTime.startTime.m += amount;
            if(replanTime.startTime.m > 59){
                replanTime.startTime.m = 0;
            }
            if(replanTime.startTime.m < 0){
                replanTime.startTime.m = 59;
            }
            this.setState({replanTime: replanTime});
        },
        changeEndHour: (amount) => {
            let replanTime = {...this.state.replanTime};
            replanTime.endTime.h += amount;
            if(replanTime.endTime.h > 23){
                replanTime.endTime.h = 0;
            }
            if(replanTime.endTime.h < 0){
                replanTime.endTime.h = 23;
            }
            this.setState({replanTime: replanTime});
        },
        changeEndMinute: (amount) => {
            let replanTime = {...this.state.replanTime};
            replanTime.endTime.m += amount;
            if(replanTime.endTime.m > 59){
                replanTime.endTime.m = 0;
            }
            if(replanTime.endTime.m < 0){
                replanTime.endTime.m = 59;
            }
            this.setState({replanTime: replanTime});
        },
        resetTime: () => {
            const replanTime = {
                startTime: {
                    h: this.props.timeObj.startTime.h,
                    m: this.props.timeObj.startTime.m
                },
                endTime: {
                    h: this.props.timeObj.endTime.h,
                    m: this.props.timeObj.endTime.m
                }
            }
            this.setState({replanTime: replanTime});
        },
        setStartTime: (h, m) => {
            let replanTime = {...this.state.replanTime};
            replanTime.startTime.m = m;
            replanTime.startTime.h = h;
            this.setState({replanTime: replanTime});
        },
        setEndTime: (h, m) => {
            let replanTime = {...this.state.replanTime};
            replanTime.endTime.m = m;
            replanTime.endTime.h = h;
            this.setState({replanTime: replanTime});
        }
    }

    replan = () => {
        this.setState({replan: false});
        this.props.replanTask(this.props.id, this.state.replanTime);
    }

    cancel = () => {
        this.setState({replan: false});
        this.timeFunctions.resetTime();
    }

    getLength = () => {
        const t = (this.props.timeObj.endTime.h*60 + this.props.timeObj.endTime.m) - (this.props.timeObj.startTime.h*60 + this.props.timeObj.startTime.m);
        if(t>59){
            return (t/60).toFixed(1)+'hrs';
        }
        else{
            return t+'min';
        }
    }

    render() { 
        if(this.state.replan){
            return (
                <div className="container-fluid bg-light rounded p-1 pt-2 pb-3 mt-1 mb-1 planTask overflow-hidden">
                    <button className="btn btn-sm d-inline btn-danger float-right" onClick={() => this.cancel()}>Cancel</button>
                    <button className="btn btn-sm d-inline btn-success float-right mr-2" onClick={() => this.replan()}>RePlan</button>
                    <p className="d-inline text-dark">{this.props.task}</p>
                    <div className="text-dark">
                        <TimeSetter time={this.state.replanTime} timeFunctions={this.timeFunctions} use24Hour={this.props.use24Hour} />
                    </div>
                </div>
            );
        }
        else{
            return (
                <div className="container-fluid bg-light rounded p-1 pt-2 pb-5 mt-1 mb-1 planTask">
                    <button className="btn btn-sm d-inline btn-warning float-right" onClick={() => this.props.unplanTask(this.props.id)}>UnPlan</button>
                    <button className="btn btn-sm d-inline btn-info float-right mr-2" onClick={() => this.setState({replan: true})}>RePlan</button>
                    <p className="d-inline text-dark">{this.props.task}</p>
                    <div>
                        <p className="float-left text-dark mt-3">{this.props.time} - {this.getLength()}</p>
                    </div>
                </div>
            );
        }
    }
}
 
export default PlannedTask;