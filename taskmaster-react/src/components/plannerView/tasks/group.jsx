import React, { Component } from 'react';

import Task from './task';

class Group extends Component {
    state = { 
        editing: false,
        newGroupName: '',
        createTask: false,
        newTask: ''
    };

    componentDidUpdate(){
        if(this.state.createTask){
            this.taskInput.focus();
        }
    }

    editGroupName = (e) => {
        if(e.key === 'Enter'){
            this.setState({editing: false});
            this.props.updateGroup(this.props.id, this.state.newGroupName);
        }
    }

    addTaskAndGroup = (e) => {
        if(e.key === 'Enter'){
            this.setState({createTask: false});
            this.props.addTaskAndGroup(this.state.newTask, this.props.id);
        }
    }

    render() { 
        if(this.state.editing){
            return (
                <div className="container-fluid rounded p-1 pt-2 pb-3 mt-1 mb-1 border border-white">
                    <button className="btn btn-sm d-inline btn-danger float-right" onClick={() => this.setState({editing: false})}>Cancel</button>
                    <button className="btn btn-sm d-inline btn-warning float-right mr-2" onClick={() => this.props.deleteGroup(this.props.id)}>Delete Group</button>
                    <input
                        type="text"
                        className="form-control mr-1 col-lg-8 d-inline"
                        id="title"
                        onChange={event => {this.setState({newGroupName: event.target.value})}}
                        onKeyPress={this.editGroupName}
                        defaultValue={this.props.groupName}
                    />
                    {this.props.tasks.map(task => <Task key={task._id} id={task._id} task={task.task} grouped={true} updateTask={this.props.updateTask} deleteTask={this.props.deleteTask} planTask={this.props.planTask} completeTask={this.props.completeTask} />)}
                </div>
            );
        }
        else if(this.state.createTask){
            return (
                <div className="container-fluid rounded p-1 pt-2 pb-3 mt-1 mb-1 border border-white">
                    <button className="btn btn-sm btn-danger float-right mb-2" onClick={() => this.setState({createTask: false})}>Cancel</button>
                    <p><i>{this.props.groupName}</i></p>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        onChange={event => {this.setState({newTask: event.target.value})}}
                        onKeyPress={this.addTaskAndGroup}
                        ref={(input) => { this.taskInput = input; }}
                    />
                    {this.props.tasks.map(task => <Task key={task._id} id={task._id} task={task.task} grouped={true} updateTask={this.props.updateTask} deleteTask={this.props.deleteTask} planTask={this.props.planTask} completeTask={this.props.completeTask} />)}
                </div>
            );
        }
        else {
            return (
                <div className="container-fluid rounded p-1 pt-2 pb-3 mt-1 mb-1 border border-white">
                    <button className="btn btn-sm d-inline btn-warning float-right" onClick={() => this.setState({editing: true})}>Edit</button>
                    <button className="btn btn-sm d-inline btn-success float-right mr-2" onClick={() => this.setState({createTask: true})}>New Task</button>
                    <p><i>{this.props.groupName}</i></p>
                    {this.props.tasks.map(task => <Task key={task._id} id={task._id} task={task.task} grouped={true} updateTask={this.props.updateTask} deleteTask={this.props.deleteTask} planTask={this.props.planTask} completeTask={this.props.completeTask} ungroup={this.props.ungroup} />)}
                </div>
            );
        }
    }
}
 
export default Group;