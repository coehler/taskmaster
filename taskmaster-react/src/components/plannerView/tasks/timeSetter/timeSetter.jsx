import React, { Component } from 'react';

import Time from './time';

class TimeSetter extends Component {
    render() { 
        return ( 
            <div className="clockMain">
                <Time time={this.props.time.startTime} use24Hour={this.props.use24Hour} changeHour={this.props.timeFunctions.changeStartHour} changeMinute={this.props.timeFunctions.changeStartMinute} setTime={this.props.timeFunctions.setStartTime} />
                <div className="clock ml-2 mr-2">to</div>
                <Time time={this.props.time.endTime} use24Hour={this.props.use24Hour} changeHour={this.props.timeFunctions.changeEndHour} changeMinute={this.props.timeFunctions.changeEndMinute} setTime={this.props.timeFunctions.setEndTime} />
            </div>
        );
    }
}
 
export default TimeSetter;