import React, { Component } from 'react';

class TextTime extends Component {
    componentDidMount(){
        this.ref.focus();
    }

    render() { 
        return (
            <input 
                type="text"
                value={this.props.textTime}
                onChange={e => this.props.setTextTime(e.target.value)}
                onBlur={this.props.saveTextTime}
                onKeyPress={e => (e.key === 'Enter') ? this.props.saveTextTime() : null}
                onFocus={e => e.target.select()}
                ref={comp => {this.ref = comp;}}
            />
        );
    }
}
 
export default TextTime;