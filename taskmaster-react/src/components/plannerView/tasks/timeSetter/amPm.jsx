import React, { Component } from 'react';

class AmPm extends Component {
    getStyles = (isAm) => {
        let style = {};
        if(this.props.hour >= 12){
            if(isAm){
                style.opacity = 0.17;
            }
            else{
                style.opacity = 1;
            }
        }
        else{
            if(isAm){
                style.opacity = 1;
            }
            else{
                style.opacity = 0.17;
            }
        }
        return style;
    }

    render() { 
        return (
            <div className="clockColumn ml-1">
                <div className="amPm" style={this.getStyles(true)}>am</div>
                <div className="amPm" style={this.getStyles(false)}>pm</div>
            </div>
        );
    }
}
 
export default AmPm;