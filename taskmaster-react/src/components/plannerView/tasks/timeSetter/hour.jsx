import React, { Component } from 'react';
import ReactScrollWheelHandler from 'react-scroll-wheel-handler';
import getStyle from './timeSetterStyles';

class Hour extends Component {
    render() { 
        if(this.props.use24Hour){
            return ( 
                <ReactScrollWheelHandler
                    upHandler={() => this.props.change(1)}
                    downHandler={() => this.props.change(-1)}
                    style={{
                        display: 'inline',
                        transition: "background-color .4s ease-out",
                    }}
                    timeout={5}
                    preventScroll={true}
                >
                    <div className="clockColumn">
                        <div className="num" style={getStyle(0, 0, this.props.time.h, false)}>0</div>
                        <div className="num" style={getStyle(0, 1, this.props.time.h, false)}>1</div>
                        <div className="num" style={getStyle(0, 2, this.props.time.h, false)}>2</div>
                    </div>
                            
                    <div className="clockColumn">
                        <div className="num" style={getStyle(1, 0, this.props.time.h, false)}>0</div>
                        <div className="num" style={getStyle(1, 1, this.props.time.h, false)}>1</div>
                        <div className="num" style={getStyle(1, 2, this.props.time.h, false)}>2</div>
                        <div className="num" style={getStyle(1, 3, this.props.time.h, false)}>3</div>
                        <div className="num" style={getStyle(1, 4, this.props.time.h, false)}>4</div>
                        <div className="num" style={getStyle(1, 5, this.props.time.h, false)}>5</div>
                        <div className="num" style={getStyle(1, 6, this.props.time.h, false)}>6</div>
                        <div className="num" style={getStyle(1, 7, this.props.time.h, false)}>7</div>
                        <div className="num" style={getStyle(1, 8, this.props.time.h, false)}>8</div>
                        <div className="num" style={getStyle(1, 9, this.props.time.h, false)}>9</div>
                    </div>
                </ReactScrollWheelHandler>
            );
        }
        else {
            return ( 
                <ReactScrollWheelHandler
                    upHandler={() => this.props.change(1)}
                    downHandler={() => this.props.change(-1)}
                    style={{
                        display: 'inline',
                        transition: "background-color .4s ease-out",
                    }}
                    timeout={5}
                    preventScroll={true}
                >
                    <div className="clockColumn">
                        <div className="num" style={getStyle(0, 0, this.props.time.h, true)}>0</div>
                        <div className="num" style={getStyle(0, 1, this.props.time.h, true)}>1</div>
                    </div>
                            
                    <div className="clockColumn">
                        <div className="num" style={getStyle(1, 0, this.props.time.h, true)}>0</div>
                        <div className="num" style={getStyle(1, 1, this.props.time.h, true)}>1</div>
                        <div className="num" style={getStyle(1, 2, this.props.time.h, true)}>2</div>
                        <div className="num" style={getStyle(1, 3, this.props.time.h, true)}>3</div>
                        <div className="num" style={getStyle(1, 4, this.props.time.h, true)}>4</div>
                        <div className="num" style={getStyle(1, 5, this.props.time.h, true)}>5</div>
                        <div className="num" style={getStyle(1, 6, this.props.time.h, true)}>6</div>
                        <div className="num" style={getStyle(1, 7, this.props.time.h, true)}>7</div>
                        <div className="num" style={getStyle(1, 8, this.props.time.h, true)}>8</div>
                        <div className="num" style={getStyle(1, 9, this.props.time.h, true)}>9</div>
                    </div>
                </ReactScrollWheelHandler>
            );
        }
    }
}
 
export default Hour;