import React, { Component } from 'react';

import AmPm from './amPm';
import Hour from './hour';
import Minute from './minute';
import TextTime from './textTime';

class Time extends Component {
    state = {
        textTime: '',
        editingText: false
    };

    renderAmPm = () => {
        if(!this.props.use24Hour){
            return <AmPm hour={this.props.time.h} />;
        }
        return null;
    }

    activateTextTime = () => {
        const paddedMins = ("00"+this.props.time.m).substr(-2);
        if(this.props.use24Hour){
            this.setState({textTime: this.props.time.h+":"+paddedMins});
        }
        else{
            let parsedHour = this.props.time.h % 12;
            if(parsedHour === 0){
                parsedHour = 12;
            }
            let amPm;
            if(this.props.time.h >= 12){
                amPm = 'pm';
            }
            else{
                amPm = 'am';
            }
            this.setState({textTime: parsedHour+":"+paddedMins+amPm});
        }
        this.setState({editingText: true});
    }

    editTextTime = (text) => {
        const regex = /^\d{0,2}:?\d{0,2}$/;
        const amPmRegex = /\d{1,2}:\d{2}((a|p)m?)?$/i;
        const startRegex = /^\d{1,2}:\d{2}/;
        const hasColon = /:/;
        if(!hasColon.test(text) && text.length >= 3){
            text = [text.slice(0, 2), ':', text.slice(2)].join('');
        }
        if(startRegex.test(text)){
            if(amPmRegex.test(text)){
                this.setState({textTime: text});
            }
        }
        else{
            if(regex.test(text)){
                this.setState({textTime: text});
            }
        }
    }

    saveTextTime = () => {
        if(this.state.textTime.length === 0){
            this.setState({editingText: false});
        }
        else{
            const regex = /^\d{1,2}:\d{2}(am|pm)?$/i;
            const amPmRegex = /(am|pm)$/i;
            if(regex.test(this.state.textTime)){
                let hours = parseInt(this.state.textTime.substr(0, this.state.textTime.indexOf(':')));
                let minutes = parseInt(this.state.textTime.substr(this.state.textTime.indexOf(':')+1, 2));
                if(amPmRegex.test(this.state.textTime)){
                    //12 hr
                    if(hours <= 12 && minutes <= 59){
                        if(hours === 12){
                            hours = 0;
                        }
                        if(!(this.state.textTime.substr(-2).toLowerCase() === 'am')){
                            hours+=12;
                        }
                        this.props.setTime(hours, minutes);
                        this.setState({editingText: false});
                    }
                }
                else{
                    //24 hr
                    if(hours <= 23 && minutes <= 59){
                        this.props.setTime(hours, minutes);
                        this.setState({editingText: false});
                    }
                }
            }
        }
    }

    render() { 
        if(this.state.editingText){
            return (
                <div className="clock">
                    <TextTime textTime={this.state.textTime} setTextTime={this.editTextTime} saveTextTime={this.saveTextTime} />
                </div>
            );
        }
        else{
            return (
                <div className="clock mr-2" onDoubleClick={this.activateTextTime}>
                    <Hour time={this.props.time} change={this.props.changeHour} use24Hour={this.props.use24Hour} />
                    <div className="clockColon"></div>
                    <Minute time={this.props.time} change={this.props.changeMinute} />
                    {this.renderAmPm()}
                </div>
            );
        }
    }
}
 
export default Time;