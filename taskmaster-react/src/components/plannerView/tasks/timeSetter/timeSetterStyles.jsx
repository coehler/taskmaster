export default (s, n, time, hour12Hour) => {
    let parsedTime = time;
    if(hour12Hour){
        parsedTime = parsedTime % 12;
        if(parsedTime === 0){
            parsedTime = 12;
        }
    }
    const n1 = Math.floor(parsedTime / 10);
    const n2 = (parsedTime % 10);

    const offset1 = n1*-1;
    const offset2 = n2*-1;

    let style = {}; 

    if(s === 0){
        if(Math.abs(n-n1) === 0){
            style.opacity = 1.0;
        }
        else if(Math.abs(n-n1) === 1){
            style.opacity = 0.17;
        }
        else{
            style.opacity = 0.0;
        }

        style.transform = 'translateY(calc(' + offset1 + 'em))';

    }
    else{
        if(Math.abs(n-n2) === 0){
            style.opacity = 1.0;
        }
        else if(Math.abs(n-n2) === 1){
            style.opacity = 0.17;
        }
        else{
            style.opacity = 0.0;
        }

        style.transform = 'translateY(calc(' + offset2 + 'em))';

    }

    return style;
}