import React, { Component } from 'react';

class GroupSetter extends Component {
    render() { 
        return (
            <div className="container-fluid bg-dark rounded p-1 pb-2 mt-3 border border-white">
                <div>
                    <p className="mt-0 mb-1">Add to Group:</p>
                    {this.props.groups.map(group => <button key={group._id} className="btn btn-sm d-inline btn-info mr-1 mb-0" onClick={() => this.props.group(this.props.taskID, group._id)}>{group.groupName}</button>)}
                </div>
            </div>
        );
    }
}
 
export default GroupSetter;