import React, { Component } from 'react';

import TimelineItem from './timelineItem';

class Timeline extends Component {

    state = {
        time: 0
    }

    constructor() {
        super();
        const timeObj = new Date();
        const time = timeObj.getHours()*60 + timeObj.getMinutes();
        this.state.time = time;
    }

    componentDidMount() {
        this.interval = setInterval(() => {
            const timeObj = new Date();
            const time = timeObj.getHours()*60 + timeObj.getMinutes();
            this.setState({time});
        }, 3000);
        this.props.gotoTime();
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    getCursor = () => {
        if(this.checkCurrentDay() === true){
            return (
                <div className="arrow-total" style={{transform: 'translateY(calc('+(this.state.time*0.1)+'em - 5px))'}}>
                    <div className="arrow" />
                    <div className="arrow-line" />
                </div>
            );
        }
        else return null;
    }

    checkCurrentDay = () => {
        const currentDate = new Date();

        if(this.props.date.year === currentDate.getFullYear() && this.props.date.month === currentDate.getMonth() && this.props.date.day === currentDate.getDate()){
            return true;
        }
        else{
            return false;
        }
    }

    calculateGroupLength = (startTime, endTime, amount) => {
        let start = startTime;
        let end = endTime;

        let tempList = [...this.props.plannedTasks].filter(plan => {
            const pStart = plan.time.startTime.h*60 + plan.time.startTime.m;
            const pEnd = plan.time.endTime.h*60 + plan.time.endTime.m;

            if((start > pStart && start < pEnd) || (end > pStart && end < pEnd) || (pStart > start && pStart < end) || (pEnd < end && pEnd > start) || (start === pStart) || (end === pEnd)){
                return true;
            }
            return false;
        });
        
        if(tempList.length > amount){
            tempList.forEach(plan => {
                const pStart = plan.time.startTime.h*60 + plan.time.startTime.m;
                const pEnd = plan.time.endTime.h*60 + plan.time.endTime.m;

                if(pStart < start){
                    start = pStart;
                }

                if(pEnd > end){
                    end = pEnd;
                }
            });
            return this.calculateGroupLength(start, end, tempList.length);
        }
        else{
            return {
                plan: tempList
            };
        }
    }

    getPlannedTasks = () => {
        let plannedTasks = [...this.props.plannedTasks];

        let plans = [];

        while(plannedTasks.length > 0){
            const toPush = this.calculateGroupLength(plannedTasks[0].time.startTime.h*60 + plannedTasks[0].time.startTime.m, plannedTasks[0].time.endTime.h*60 + plannedTasks[0].time.endTime.m, 1);
            plannedTasks = plannedTasks.filter(plan => {
                if(toPush.plan.includes(plan)){
                    return false;
                }
                return true;
            });
            plans.push(toPush);
        }

        return plans;
    }

    getMargin = () => {
        if(this.props.use24Hour){
            return "ml-4";
        }
        return "ml-5";
    }

    getAbsoluteClass = () => {
        if(this.props.use24Hour){
            return "absolutePlannedTask24H";
        }
        return "absolutePlannedTask12H";
    }

    render() {
        return (
            <div className="container-fluid col" style={{overflow: 'hidden'}}>
                <nav className="navbar navbar-dark bg-dark sticky-top p-0 pl-4 pr-4">
                    <button className="btn btn-secondary btn-sm m-2 d-inline" onClick={() => this.props.dateFunctions.changeMonth(-1)}>&lt;&lt;</button>
                    <button className="btn btn-secondary btn-sm m-2 d-inline" onClick={() => this.props.dateFunctions.changeDay(-1)}>&lt;</button>
                    <button className="btn btn-secondary btn-sm m-2 d-inline" onClick={this.props.dateFunctions.resetDate}>{this.props.date.year+' - '+(this.props.date.month+1)+' - '+this.props.date.day}</button>
                    <button className="btn btn-secondary btn-sm m-2 d-inline" onClick={() => this.props.dateFunctions.changeDay(1)}>&gt;</button>
                    <button className="btn btn-secondary btn-sm m-2 d-inline" onClick={() => this.props.dateFunctions.changeMonth(1)}>&gt;&gt;</button>
                </nav>

                <div className="scrollWindows">
                    <div className="relative">

                        <div className="timeline">
                            {this.props.halfHours}
                        </div>

                        <div className={"pl-3 "+this.getMargin()}>
                            {this.getPlannedTasks().map(planGroup => <div className={this.getAbsoluteClass()} key={Math.random()}>{planGroup.plan.map(plan => <TimelineItem key={plan._id} task={plan.task} time={plan.time} currentTime={this.state.time} amount={planGroup.plan.length} isCurrentDay={this.checkCurrentDay()} use24Hour={this.props.use24Hour} />)}</div>)}
                        </div>

                        {this.getCursor()}
                    </div>
                    <div className="scrollFull" />
                </div>
            </div>
        );
    }
}
 
export default Timeline;