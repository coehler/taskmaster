import React, { Component } from 'react';

import Notify from '../../../scripts/notify';
import timeConverter from '../../../scripts/timeConverter';

class TimelineItem extends Component {

    state = {
        notified: false
    }

    getStyle = () => {
        let style = {}

        const start = this.props.time.startTime.h*60 + this.props.time.startTime.m;
        let end = this.props.time.endTime.h*60 + this.props.time.endTime.m;

        //Temp fix until multi-day planning is implemented
        if(end < start){
            end += (24*60);
        }

        //Height
        style.height = 'calc('+Math.abs(end-start)+' * 0.1em)';

        //Transform
        style.transform = 'translateY(calc('+start+' * 0.1em))';

        //Width
        style.width = 'calc(calc((1/'+this.props.amount+') * 100%) - 1px)'

        return style;
    }

    getColor = () => {
        const start = this.props.time.startTime.h*60 + this.props.time.startTime.m;
        const end = this.props.time.endTime.h*60 + this.props.time.endTime.m;

        if(this.props.currentTime >= start && this.props.currentTime <= end && this.props.isCurrentDay === true){
            return 'success';
        }
        else{
            return 'secondary';
        }
    }

    getFormatedTime = () => {
        return timeConverter.convertTimeToDisplay(this.props.use24Hour, this.props.time.startTime.h, this.props.time.startTime.m)+' to '+timeConverter.convertTimeToDisplay(this.props.use24Hour, this.props.time.endTime.h, this.props.time.endTime.m);
    }

    componentDidUpdate(){
        if(this.props.currentTime === (this.props.time.startTime.h*60 + this.props.time.startTime.m) && this.state.notified === false){
            Notify.notify(this.props.task+'\n'+timeConverter.convertTimeToDisplay(this.props.use24Hour, this.props.time.startTime.h, this.props.time.startTime.m)+' to '+timeConverter.convertTimeToDisplay(this.props.use24Hour, this.props.time.endTime.h, this.props.time.endTime.m));
            this.setState({notified: true});
        }
    }
    
    render() { 
        return (
            <div className="container-fluid bg-info rounded timeline-item m-0 p-1" style={this.getStyle()}>
                <span className={"badge d-inline float-right m-1 badge-"+this.getColor()}>{this.getFormatedTime()}</span>
                <p className="d-inline">{this.props.task}</p>
            </div>
        );
    }
}
 
export default TimelineItem;