import React, { Component } from 'react';
import axios from 'axios';

import Tasks from './tasks/tasks';
import Timeline from './timeline/timeline';

import Notify from '../../scripts/notify';
import timeConverter from '../../scripts/timeConverter';

class PlannerView extends Component {
    state = {
        tasks: [],
        plannedTasks: [],
        completedTasks: [],
        groups: [],
        date: {},
        timelineRefs: [],
        halfHours: []
    }

    constructor(props) {
        super(props);
        const curDate = new Date();
        const date = {
            day: curDate.getDate(),
            month: curDate.getMonth(),
            year: curDate.getFullYear()
        }
        this.state.date = date;

        for(let i = 0; i < (60*24); i+=30){
            let hour = Math.floor(i/60);
            let minute = i%60;
            this.state.timelineRefs.push(React.createRef());
            this.state.halfHours.push(<div className="halfhour" key={i} ref={this.state.timelineRefs[this.state.timelineRefs.length - 1]}>{timeConverter.convertTimeToDisplay(this.props.use24Hour, hour, minute)}</div>);
        }
    }

    toUrlEncoded = (obj) => Object.keys(obj).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(obj[k])).join('&');

    getTasks = () => {
        axios.get('/api/task')
            .then(res => {
                if(res.data === false){
                    this.props.redirect();
                }
                else if(res.data === 'err'){
                    this.props.throwError('Error: Unable to get from server. ERR: GET_TASKS');
                }
                else{
                    this.setState({tasks: res.data});
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to get from server. ERR: GET_TASKS');
            });
    }

    getPlan = () => {
        axios.get('/api/plan?'+this.toUrlEncoded({...this.state.date}))
            .then(res => {
                if(res.data === false){
                    this.props.redirect();
                }
                else if(res.data === 'err'){
                    this.props.throwError('Error: Unable to get from server. ERR: GET_PLAN');
                }
                else{
                    let sortedData = [...res.data];
                    sortedData.sort((a,b) => (a.time.startTime.h*60+a.time.startTime.m)-(b.time.startTime.h*60+b.time.startTime.m));
                    this.setState({plannedTasks: sortedData});
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to get from server. ERR: GET_PLAN');
            });
    }

    getCompleted = () => {
        axios.get('/api/done?'+this.toUrlEncoded({...this.state.date}))
            .then(res => {
                if(res.data === false){
                    this.props.redirect();
                }
                else if(res.data === 'err'){
                    this.props.throwError('Error: Unable to get from server. ERR: GET_COMPLETED');
                }
                else{
                    this.setState({completedTasks: res.data});
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to get from server. ERR: GET_COMPLETED');
            });
    }

    getGroups = () => {
        axios.get('/api/groups')
            .then(res => {
                if(res.data === false){
                    this.props.redirect();
                }
                else if(res.data === 'err'){
                    this.props.throwError('Error: Unable to get from server. ERR: GET_GROUPS');
                }
                else{
                    this.setState({groups: res.data});
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to get from server. ERR: GET_GROUPS');
            });
    }

    addTask = (task) => {
        axios.post('/api/task', 'task='+encodeURIComponent(task), {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(res => {
                if(res.data === true){
                    this.getTasks();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: ADD_TASK');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to post to server. ERR: ADD_TASK');
            });
    }

    updateTask = (id, task) => {
        axios.put('/api/task/'+id, 'task='+encodeURIComponent(task), {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(res => {
                if(res.data === true){
                    this.getTasks();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: UPDATE_TASK');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to update task. ERR: UPDATE_TASK');
            });
    }

    deleteTask = (id) => {
        axios.delete('/api/task/'+id)
            .then(res => {
                if(res.data === true){
                    this.getTasks();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: DELETE_TASK');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to delete from server. ERR: DELETE_TASK');
            });
    }

    plan = (id, time) => {
        const timeObj = {
            startHour: time.startTime.h,
            startMinute: time.startTime.m,
            endHour: time.endTime.h,
            endMinute: time.endTime.m
        };
        const dateObj = {
            startDay: this.state.date.day,
            startMonth: this.state.date.month,
            startYear: this.state.date.year,
            endDay: this.state.date.day,
            endMonth: this.state.date.month,
            endYear: this.state.date.year
        };
        axios.put('/api/plan/'+id, this.toUrlEncoded(dateObj)+'&'+this.toUrlEncoded(timeObj), {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(res => {
                if(res.data === true){
                    this.getTasks();
                    this.getPlan();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: PLAN_TASK');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to update task. ERR: PLAN_TASK');
            });
    }

    unPlan = (id) => {
        axios.put('/api/unplan/'+id)
            .then(res => {
                if(res.data === true){
                    this.getTasks();
                    this.getPlan();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: UNPLAN_TASK');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to update task. ERR: UNPLAN_TASK');
            });
    }

    replan = (id, time) => {
        const timeObj = {
            startHour: time.startTime.h,
            startMinute: time.startTime.m,
            endHour: time.endTime.h,
            endMinute: time.endTime.m
        };
        const dateObj = {
            startDay: this.state.date.day,
            startMonth: this.state.date.month,
            startYear: this.state.date.year,
            endDay: this.state.date.day,
            endMonth: this.state.date.month,
            endYear: this.state.date.year
        };
        axios.put('/api/replan/'+id, this.toUrlEncoded(dateObj)+'&'+this.toUrlEncoded(timeObj), {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(res => {
                if(res.data === true){
                    this.getPlan();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: REPLAN_TASK');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to update task. ERR: REPLAN_TASK');
            });
    }

    completed = (id) => {
        axios.put('/api/done/'+id, this.toUrlEncoded({...this.state.date}), {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(res => {
                if(res.data === true){
                    this.getTasks();
                    this.getCompleted();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: COMPLETE_TASK');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to update task. ERR: COMPLETE_TASK');
            });
    }

    undo = (id) => {
        axios.put('/api/undone/'+id)
            .then(res => {
                if(res.data === true){
                    this.getTasks();
                    this.getCompleted();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: UNDO_COMP_TASK');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to update task. ERR: UNDO_COMP_TASK');
            });
    }

    gotoTime = () => {
        const currentDate = new Date();
        if(this.state.date.year === currentDate.getFullYear() && this.state.date.month === currentDate.getMonth() && this.state.date.day === currentDate.getDate()){
            const time = currentDate.getHours()*60 + currentDate.getMinutes();
            let index = Math.floor(time/30)-5;
            if(index<0) index = 0;


            this.state.timelineRefs[index].current.scrollIntoView({behavior: 'smooth', block: 'start'});
        }
        else{
            this.state.timelineRefs[0].current.scrollIntoView({behavior: 'smooth', block: 'start'});
        }
    }

    deleteGroup = (id) => {
        axios.delete('/api/groups/'+id)
            .then(res => {
                if(res.data === true){
                    this.getGroups();
                    this.getTasks();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: DELETE_GROUP');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to delete from server. ERR: DELETE_GROUP');
            });
    }

    updateGroup = (id, groupName) => {
        axios.put('/api/groups/'+id, 'groupName='+encodeURIComponent(groupName), {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(res => {
                if(res.data === true){
                    this.getGroups();
                    this.getTasks();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: UPDATE_GROUP');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to update group. ERR: UPDATE_GROUP');
            });
    }

    ungroup = (id) => {
        axios.put('/api/ungroup/'+id, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(res => {
                if(res.data === true){
                    this.getTasks();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: UNGROUP');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to update group. ERR: UNGROUP');
            });
    }

    addGroup = (groupName) => {
        axios.post('/api/groups', 'groupName='+groupName, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(res => {
                if(res.data === true){
                    this.getGroups();
                    this.getTasks();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: ADD_GROUP');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to add group. ERR: ADD_GROUP');
            });
    }

    group = (taskID, groupID) => {
        axios.put('/api/group/'+groupID+'/'+taskID, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(res => {
                if(res.data === true){
                    this.getTasks();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: GROUP');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to update group. ERR: GROUP');
            });
    }

    addTaskAndGroup = (task, groupID) => {
        axios.post('/api/taskngroup/'+groupID, 'task='+task, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(res => {
                if(res.data === true){
                    this.getTasks();
                }
                else if(res.data === false){
                    this.props.redirect();
                }
                else{
                    this.props.throwError('Error: Server gave bad response. ERR: ADD_TASK_AND_GROUP');
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to add task. ERR: ADD_TASK_AND_GROUP');
            });
    }

    dateFunctions = {
        changeDay: (amount) => {
            let dateObj = new Date();
            dateObj.setDate(this.state.date.day);
            dateObj.setMonth(this.state.date.month);
            dateObj.setFullYear(this.state.date.year);
            dateObj.setDate(dateObj.getDate()+amount);
            const date = {
                day: dateObj.getDate(),
                month: dateObj.getMonth(),
                year: dateObj.getFullYear()
            }
            this.setState({date: date});
            setTimeout(this.getPlan, 10);
            setTimeout(this.getCompleted, 10);
            setTimeout(this.gotoTime, 10);
        },
        changeMonth: (amount) => {
            let dateObj = new Date();
            dateObj.setDate(this.state.date.day);
            dateObj.setMonth(this.state.date.month);
            dateObj.setFullYear(this.state.date.year);
            dateObj.setMonth(dateObj.getMonth()+amount);
            const date = {
                day: dateObj.getDate(),
                month: dateObj.getMonth(),
                year: dateObj.getFullYear()
            }
            this.setState({date: date});
            setTimeout(this.getPlan, 10);
            setTimeout(this.getCompleted, 10);
            setTimeout(this.gotoTime, 10);
        },
        resetDate: () => {
            const curDate = new Date();
            const date = {
                day: curDate.getDate(),
                month: curDate.getMonth(),
                year: curDate.getFullYear()
            }
            this.setState({date: date});
            setTimeout(this.getPlan, 10);
            setTimeout(this.getCompleted, 10);
            setTimeout(this.gotoTime, 10);
        }
    }

    componentDidMount() {
        this.getTasks();
        this.getGroups();
        this.getPlan();
        this.getCompleted();
        Notify.requestPermission();
    }

    render() { 
        return (  
            <div className="row no-gutters p-1">
                <Tasks tasks={this.state.tasks} plannedTasks={this.state.plannedTasks} completedTasks={this.state.completedTasks} addTask={this.addTask} updateTask={this.updateTask} deleteTask={this.deleteTask} planTask={this.plan} unplanTask={this.unPlan} completeTask={this.completed} undoTask={this.undo} replanTask={this.replan} groups={this.state.groups} addGroup={this.addGroup} group={this.group} updateGroup={this.updateGroup} deleteGroup={this.deleteGroup} ungroup={this.ungroup} addTaskAndGroup={this.addTaskAndGroup} use24Hour={this.props.use24Hour} />
                <Timeline date={this.state.date} dateFunctions={this.dateFunctions} plannedTasks={this.state.plannedTasks} refs={this.state.timelineRefs} halfHours={this.state.halfHours} gotoTime={this.gotoTime} use24Hour={this.props.use24Hour} />
            </div>
        );
    }
}
 
export default PlannerView;