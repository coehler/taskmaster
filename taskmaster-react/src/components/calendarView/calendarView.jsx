import React, { Component } from 'react';
import axios from 'axios';

import CalendarItem from './calendarItem';

class CalendarView extends Component {
    state = {
        selected: {
            month: 0,
            year: 2020,
            numDays: 0,
            startDay: 0
        },
        currentDay: {
            day: 1,
            month: 0,
            year: 2020
        },
        tasks: []
    }

    months = [
        'January',
        'Febuary',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];

    toUrlEncoded = (obj) => Object.keys(obj).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(obj[k])).join('&');

    componentDidMount() {
        const currentDate = new Date();
        this.setState({
            selected: {
                month: currentDate.getMonth(),
                year: currentDate.getFullYear(),
                numDays: new Date(currentDate.getFullYear(), currentDate.getMonth()+1, 0).getDate(),
                startDay: new Date(currentDate.getFullYear(), currentDate.getMonth(), 1).getDay()
            },
            currentDay: {
                day: currentDate.getDate(),
                month: currentDate.getMonth(),
                year: currentDate.getFullYear()
            }
        });
        setTimeout(this.getCalendar, 10);
    }

    getCalendar = () => {
        axios.get('/api/calendar?'+this.toUrlEncoded({month: this.state.selected.month, year: this.state.selected.year}))
            .then(res => {
                if(res.data === false){
                    this.props.redirect();
                }
                else if(res.data === 'err'){
                    this.props.throwError('Error: Unable to get from server. ERR: GET_CALENDAR');
                }
                else{
                    this.setState({tasks: res.data});
                }
            })
            .catch(err => {
                this.props.throwError('Error: Unable to get from server. ERR: GET_CALENDAR');
            });
    }

    changeMonth = (i) => {
        let month = this.state.selected.month;
        let year = this.state.selected.year;
        month += i;
        if(month > 11){
            month = 0;
            year ++;
        }
        else if(month < 0){
            month = 11;
            year --;
        }

        this.setState({
            selected: {
                month: month,
                year: year,
                numDays: new Date(year, month+1, 0).getDate(),
                startDay: new Date(year, month, 1).getDay()
            }
        });
        setTimeout(this.getCalendar, 10);
    }

    resetDate = () => {
        const currentDate = new Date();
        this.setState({
            selected: {
                month: currentDate.getMonth(),
                year: currentDate.getFullYear(),
                numDays: new Date(currentDate.getFullYear(), currentDate.getMonth()+1, 0).getDate(),
                startDay: new Date(currentDate.getFullYear(), currentDate.getMonth(), 1).getDay()
            }
        });
        setTimeout(this.getCalendar, 10);
    }

    getTasks = (i) => {
       const t = this.state.tasks.filter(day => day.day === i);
       if(t.length > 0){
            return t[0].tasks;
       }
       else{
           return [];
       }
    }

    getContent = () => {
        let content = [];

        for(let i = this.state.selected.startDay; i>0; i--){
            content.push(
                <CalendarItem key={31+i} />
            );
        }

        for(let i = 1; i<=this.state.selected.numDays; i++){
            content.push(
                <CalendarItem key={i} date={{day: i, month: this.state.selected.month, year: this.state.selected.year}} currentDay={this.state.currentDay} tasks={this.getTasks(i)} />
            );
        }

        return content;
    }

    render() { 
        return (
            <div>
                <nav className="navbar navbar-dark bg-secondary sticky-top p-0 pl-4 pr-4">
                    <div style={{textAlign: 'center', width: '100%'}}>
                        <button className="btn btn-dark btn-sm m-2 d-inline" onClick={() => this.changeMonth(-1)}>&lt;</button>
                        <button className="btn btn-dark btn-sm m-2 d-inline" style={{position: 'relative', width: '10%'}} onClick={this.resetDate}>{this.months[this.state.selected.month]+' '+this.state.selected.year}</button>
                        <button className="btn btn-dark btn-sm m-2 d-inline" onClick={() => this.changeMonth(1)}>&gt;</button>
                    </div>
                </nav>
                <div className="ml-5 mr-5">
                    <div className="calendar calendar-top">Sunday</div>
                    <div className="calendar calendar-top">Monday</div>
                    <div className="calendar calendar-top">Tuesday</div>
                    <div className="calendar calendar-top">Wednesday</div>
                    <div className="calendar calendar-top">Thursday</div>
                    <div className="calendar calendar-top">Friday</div>
                    <div className="calendar calendar-top">Saturday</div>
                    <div className="calendar-container">
                        {this.getContent()}
                    </div>
                </div>
            </div>
        );
    }
}
 
export default CalendarView;