import React, { Component } from 'react';

import PlannedCalItem from './plannedCalItem';

class CalendarItem extends Component {

    getContent = () => {
        if(typeof this.props.date !== 'undefined'){
            return (
                <React.Fragment>
                    <p className="calendar-num">{this.props.date.day}</p>
                    <div className="calendar-content">
                        {this.props.tasks.map(task => <PlannedCalItem key={task._id} task={task.task} />)}
                    </div>
                </React.Fragment>
            );
        }
        return null;
    }

    getStyle = () => {
        let style = {};

        if(typeof this.props.date !== 'undefined'){
            if(this.props.date.day === this.props.currentDay.day && this.props.date.month === this.props.currentDay.month && this.props.date.year === this.props.currentDay.year){
                style.border = '1px solid #00bc8c';
                style.background = '#00291e';
            }
        }

        return style;
    }

    render() { 
        return (  
            <div className="calendar calendar-item m-0" style={this.getStyle()}>
                {this.getContent()}
            </div>
        );
    }
}
 
export default CalendarItem;