import React, { Component } from 'react';

class PlannedCalItem extends Component {
    render() { 
        return (  
            <div className="container-fluid bg-info rounded m-0 mb-1 p-1">
                <p className="m-0">{this.props.task}</p>
            </div>
        );
    }
}
 
export default PlannedCalItem;