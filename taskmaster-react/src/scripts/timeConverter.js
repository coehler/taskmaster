const padNumber = (number) => {
    if(number < 10){
        return '0'+number;
    }
    else {
        return number;
    }
}

export default {
    convertTimeToDisplay: (use24Hour, hours, minutes) => {
        if(use24Hour){
            return hours+':'+padNumber(minutes);
        }
        else{
            let amPm;
            if(hours >= 12){
                amPm = 'pm';
            }
            else {
                amPm = 'am';
            }
            let parsedHours = hours%12;
            if(parsedHours === 0){
                parsedHours = 12;
            }
            return parsedHours+':'+padNumber(minutes)+amPm;
        }
    }
}