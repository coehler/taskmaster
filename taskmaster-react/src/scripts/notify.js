const url = 'https://taskmaster.ignite.coehler.ca';

export default {

    notify(text){
        if(this.requestPermission()){
            this.doNotification(text);
        }
    },

    requestPermission(){
        if('Notification' in window){
            if(Notification.permission === 'granted'){
                return true;
            }
            else if(Notification.permission !== 'denied'){
                Notification.requestPermission(result => {
                    if(result === 'granted'){
                        return true;
                    }
                });
            }
        }
        return false;
    },

    doNotification(text){
        let notification = new Notification('Taskmaster', {
            icon: url+'/assets/logo.png',
            body: text
        });

        notification.onclick = function() {
            window.open(url);
        }

        setTimeout(notification.close.bind(notification), 10000);
    }
}