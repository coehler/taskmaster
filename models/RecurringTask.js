const mongoose = require('mongoose');

const RecurringTaskSchema = new mongoose.Schema({
    username: {
       type: String,
       required: true 
    },
    task: {
        type: String,
        required: true
    },
    time: {
        type: Object,
        required: true
    },
    //Also used as the date if recurring yearly
    startDate: {
        type: Object,
        required: true
    },
    //If no end date
    endless: {
        type: Boolean,
        default: true
    },
    endDate: {
        type: Object,
    },
    //All information on when to repeat
    repeat: {
        type: Object,
        required: true
    },
    timestamp: {
        type: Date,
        default: Date.now
    }
});

const RecurringTask = mongoose.model('RecurringTask', RecurringTaskSchema);

module.exports = RecurringTask;