const mongoose = require('mongoose');

const GroupSchema = new mongoose.Schema({
    username: {
       type: String,
       required: true 
    },
    groupName: {
        type: String,
        required: true
    },
    tasks: {
        type: Array,
        default: []
    },
    timestamp: {
        type: Date,
        default: Date.now
    }
});

const Group = mongoose.model('Group', GroupSchema);

module.exports = Group;