const mongoose = require('mongoose');

const PlannedTaskSchema = new mongoose.Schema({
    username: {
       type: String,
       required: true 
    },
    task: {
        type: String,
        required: true
    },
    time: {
        type: Object,
        required: true
    },
    startDate: {
        type: Object,
        required: true
    },
    endDate: {
        type: Object,
        required: true
    },
    timestamp: {
        type: Date,
        default: Date.now
    }
});

const PlannedTask = mongoose.model('PlannedTask', PlannedTaskSchema);

module.exports = PlannedTask;