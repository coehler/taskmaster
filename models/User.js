const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    username: {
       type: String,
       required: true 
    },
    password: {
        type: String,
        required: true
    },
    admin: {
        type: Boolean,
        default: false
    },
    use24Hour: {
        type: Boolean,
        default: true
    },
    timestamp: {
        type: Date,
        default: Date.now
    }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;