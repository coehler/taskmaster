const mongoose = require('mongoose');

const CompletedTaskSchema = new mongoose.Schema({
    username: {
       type: String,
       required: true 
    },
    task: {
        type: String,
        required: true
    },
    date: {
        type: Object,
        required: true
    },
    timestamp: {
        type: Date,
        default: Date.now
    }
});

const CompletedTask = mongoose.model('CompletedTask', CompletedTaskSchema);

module.exports = CompletedTask;