const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
    username: {
       type: String,
       required: true 
    },
    task: {
        type: String,
        required: true
    },
    timestamp: {
        type: Date,
        default: Date.now
    }
});

const Task = mongoose.model('Task', TaskSchema);

module.exports = Task;