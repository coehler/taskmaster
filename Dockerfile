FROM node

COPY . .

RUN npm install

RUN npm run install-react
RUN npm run build

CMD [ "npm", "start" ]
