const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const session = require('express-session');
const flash = require('connect-flash');
const cors = require('cors');
const passport = require('passport');
const enforce = require('express-sslify');

require('dotenv').config();
if(typeof process.env.MONGO_URI === 'undefined')
    throw 'ERROR: Missing env var MONGO_URI';
if(!process.env.SECRET === 'undefined')
    throw 'ERROR: Missing env var SECRET';

const app = express();

require('./config/passport')(passport);

//Mongoose
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Connected to MongoDB'))
    .catch(err => console.log(err));

//EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');
app.use(express.urlencoded({extended: false}));

//SSL
if(process.env.FORCE_HTTPS === 'true'){
    app.use(enforce.HTTPS({ trustProtoHeader: true }));
}

//Express Session
app.use(session({
    secret: process.env.SECRET,
    resave: true,
    saveUninitialized: true
}));

//Passport
app.use(passport.initialize());
app.use(passport.session());

//Cors
app.use(cors());

//Flash
app.use(flash());

//Global Vars
app.use((req, res, next) => {
    res.locals.error = req.flash('error');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.success_msg = req.flash('success_msg');
    next();
});

//Authentication
const auth = require('./config/auth');

//Routes
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));
app.use('/api', require('./routes/api'));

//Static Routes
app.use('/assets', express.static('./assets'));

app.use('/', auth.ensureAuthenticated);
app.use('/', express.static('./taskmaster-react/build'));

//Listen
const port = process.env.PORT || 5000;
app.listen(port, console.log(`Listening on port ${port}`));