module.exports = {
    ensureAuthenticated: function(req, res, next) {
        if(req.isAuthenticated()){
            return next();
        }
        let query = '';
        if(req.originalUrl !== '/'){
            query = '?r='+encodeURIComponent(req.originalUrl.substring(1));
        }
        res.redirect('/login'+query);
    },
    ensureAuthenticatedAdmin: function(req, res, next) {
        if(req.isAuthenticated() && req.user.admin){
            return next();
        }
        else if(!req.isAuthenticated()){
            let query = '';
            if(req.originalUrl !== '/'){
                query = '?r='+encodeURIComponent(req.originalUrl.substring(1));
            }
            res.redirect('/login'+query);
        }
        else{
            req.flash('error_msg', 'You do not have the required permissions to view that resource!');
            res.redirect('/');
        }
    },
    ensureAuthenticatedAPI: function(req, res, next) {
        if(req.isAuthenticated()){
            return next();
        }
        res.send(false);
    }
}