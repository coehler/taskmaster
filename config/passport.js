const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');

const User = require('../models/User');

module.exports = function(passport) {
    passport.use(
        new LocalStrategy({ usernameField: 'username' }, (username, password, done) => {

            //Check if user exists
            User.findOne({ username: username })
                .then(user => {
                    if(!user) {
                        return done(null, false, { message: 'Username or password is incorrect!' });
                    }

                    if(process.env.CHECK_PASS === 'false'){
                        return done(null, user);
                    }

                    //Check password
                    bcrypt.compare(password, user.password, (err, isMatch) => {
                        if(err) throw err;

                        if(isMatch){
                            return done(null, user);
                        }
                        else {
                           return done(null, false, { message: 'Username or password is incorrect!' }); 
                        }
                    });

                });
        })
    );

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, (err, user) => {
            done(err, user);
        });
    });
}