const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');

const auth = require('../config/auth');
const MESSAGES = require('../config/messages');
const VERSION = require('../config/version');

const User = require('../models/User');
const Task = require('../models/Task');
const PlannedTask = require('../models/PlannedTask');
const CompletedTask = require('../models/CompletedTask');
const Group = require('../models/Group');

//EVERYTHING HERE REQUIRES ADMIN

router.get('/', auth.ensureAuthenticatedAdmin, (req, res) => {
    User.find()
        .then(users => {
            res.render('users', {
                users,
                user: req.user,
                version: VERSION
            });
        })
        .catch(err => console.log(err));
});

router.get('/register', auth.ensureAuthenticatedAdmin, (req, res) => {
    res.render('register', {
        user: req.user,
        version: VERSION
    });
});

router.post('/register', auth.ensureAuthenticatedAdmin, (req, res) => {
    const {username, password, password2} = req.body;
    let errors = [];

    if(!username || !password || !password2){
        errors.push({msg: 'Please fill all fields!'});
    }

    if(password !== password2){
        errors.push({msg: 'Passwords do not match!'});
    }

    if(password.length < 6) {
        errors.push({msg: 'Password must be at least 6 characters long!'});
    }

    if(errors.length > 0){
        res.render('register', {
            errors,
            username,
            user: req.user,
            version: VERSION
        });
    }
    else{
        User.findOne({ username: username })
            .then(user => {
                if(user){
                    errors.push({msg: 'Username already in use!'});
                    res.render('register', {
                        errors,
                        username,
                        user: req.user,
                        version: VERSION
                    });
                }
                else{
                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(password, salt, (err, hashPass) => {
                            if(err) throw err;
                            
                            const newUser = new User({
                                username,
                                password: hashPass
                            });

                            newUser.save()
                                .then(user => {
                                    const welcomeTask = new Task({
                                        username: username,
                                        task: MESSAGES.WELCOME_MSG
                                    });
                                    welcomeTask.save()
                                        .then(() => {
                                            req.flash('success_msg', 'User '+username+' registered successfully!');
                                            res.redirect('/users');
                                        })
                                        .catch(err => console.log(err));
                                })
                                .catch(err => console.log(err));
                        });
                    });
                }
            })
            .catch(err => console.log(err));
    }
});

router.get('/:id/perms', auth.ensureAuthenticatedAdmin, (req, res) => {
    User.findById(req.params.id)
        .then(user => {
            user.admin = !user.admin;
            user.save()
                .then(user => {
                    req.flash('success_msg', 'Permissions for '+user.username+' successfully updated!');
                    res.redirect('/users');
                })
                .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
});

router.get('/:id/delete', auth.ensureAuthenticatedAdmin, (req, res) => {
    User.findById(req.params.id)
        .then(userCheck => {
            if(userCheck.admin){
                req.flash('error_msg', 'Admin users cannot be deleted!');
                res.redirect('/users');
            }
            else{
                User.findByIdAndDelete(req.params.id)
                    .then((user) => {
                        Task.deleteMany({username: user.username})
                            .then(result1 => {
                                PlannedTask.deleteMany({username: user.username})
                                    .then(result2 => {
                                        CompletedTask.deleteMany({username: user.username})
                                            .then(result3 => {
                                                Group.deleteMany({username: user.username})
                                                        .then(result4 => {
                                                            req.flash('success_msg', 'User ' + user.username + ' successfully deleted!');
                                                            res.redirect('/users');
                                                        })
                                                        .catch(err => console.log(err));
                                            })
                                            .catch(err => console.log(err));
                                    })
                                    .catch(err => console.log(err));
                            })
                            .catch(err => console.log(err));
                    })
                    .catch(err => console.log(err));
            }
        })
        .catch(err => console.log(err));
});

function compare(a, b){
    const q1 = (a.date.year*10000) + (a.date.month*100) + a.date.day;
    const q2 = (b.date.year*10000) + (b.date.month*100) + b.date.day;
    return q2-q1;
}

function timeToString(use24Hour, time, startDate, endDate){
    let sm = time.startTime.m;
    let em = time.endTime.m;
    if(sm < 10){
        sm = '0'+sm;
    }
    if(em < 10){
        em = '0'+em;
    }

    let sh = time.startTime.h;
    let eh = time.endTime.h;
    if(use24Hour){
        return sh+':'+sm+'-'+eh+':'+em;
    }
    else{
        let sAmPm;
        let eAmPm;
        if(sh >= 12){
            sAmPm = 'pm';
        }
        else{
            sAmPm = 'am';
        }
        if(eh >= 12){
            eAmPm = 'pm';
        }
        else{
            eAmPm = 'am';
        }
        sh = sh%12;
        eh = eh%12;
        if(sh === 0){
            sh = 12;
        }
        if(eh === 0){
            eh = 12;
        }
        return "["+startDate.year+"-"+(startDate.month+1)+"-"+startDate.day+" "+sh+':'+sm+sAmPm+'] - ['+endDate.year+"-"+(endDate.month+1)+"-"+endDate.day+" "+eh+':'+em+eAmPm+"]";
    }
}

router.get('/:id', auth.ensureAuthenticatedAdmin, (req, res) => {
    User.findById(req.params.id)
        .then(user => {
            if(user){
                const {username, admin} = user;
                Task.find({username: username})
                    .then(tasks => {
                        PlannedTask.find({username: username})
                            .then(plans => {
                                CompletedTask.find({username: username})
                                    .then(completed => {
                                        Group.find({username: username})
                                            .then(groups => {
                                                tasks.forEach(task => {
                                                    const groupName = groups.filter(g => g.tasks.indexOf(task._id) !== -1)[0];
                                                    if(groupName){
                                                        task.groupName = groupName.groupName;
                                                    }
                                                    else{
                                                        task.groupName = null;
                                                    }
                                                });

                                                let days = [];

                                                plans.forEach(plan => {
                                                    const index = days.findIndex(e => (e.date.year === plan.startDate.year) && (e.date.month === plan.startDate.month) && (e.date.day === plan.startDate.day));
                                                    if(index === -1){
                                                        days.push({
                                                            date: plan.startDate,
                                                            plan: [
                                                                {task: plan.task, time: timeToString(req.user.use24Hour, plan.time, plan.startDate, plan.endDate)}
                                                            ],
                                                            completed: []
                                                        });
                                                    }
                                                    else{
                                                        days[index].plan.push({task: plan.task, time: timeToString(req.user.use24Hour, plan.time, plan.startDate, plan.endDate)});
                                                    }
                                                });

                                                completed.forEach(complete => {
                                                    const index = days.findIndex(e => (e.date.year === complete.date.year) && (e.date.month === complete.date.month) && (e.date.day === complete.date.day));
                                                    if(index === -1){
                                                        days.push({
                                                            date: complete.date,
                                                            plan: [],
                                                            completed: [
                                                                {task: complete.task}
                                                            ]
                                                        });
                                                    }
                                                    else{
                                                        days[index].completed.push({task: complete.task});
                                                    }
                                                });

                                                days.sort(compare);

                                                res.render('view', {
                                                    user: req.user,
                                                    username,
                                                    admin,
                                                    tasks,
                                                    days,
                                                    version: VERSION
                                                });
                                            })
                                            .catch(err => console.log(err));
                                    })
                                    .catch(err => console.log(err));
                            })
                            .catch(err => console.log(err));
                    })
                    .catch(err => console.log(err));
            }
            else{
                req.flash('error_msg', 'User not found!');
                res.redirect('/users');
            }
        })
        .catch(err => console.log(err));
});

module.exports = router;