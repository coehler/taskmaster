const express = require('express');
const router = express.Router();

const auth = require('../config/auth');
const VERSION = require('../config/version');

const Task = require('../models/Task');
const PlannedTask = require('../models/PlannedTask');
const CompletedTask = require('../models/CompletedTask');
const Group = require('../models/Group');

async function removeTaskFromGroups(username, taskID, callback) {
    await Group.find({username: username, tasks: taskID})
        .then(groups => {
            groups.forEach(g => {
                g.tasks.splice(g.tasks.indexOf(taskID),1);
                g.save()
                    .then(() => {
                        console.log(`Removed task ${taskID} from group ${g._id} for user ${username}`);
                    })
                    .catch(err => {
                        console.log(err)
                        callback('err');
                    });
            });
            callback(true);
        })
        .catch(err => {
            console.log(err)
            callback('err');
        });
}

router.get('/user', auth.ensureAuthenticatedAPI, (req, res) => {
    res.send({
        username: req.user.username,
        admin: req.user.admin,
        use24Hour: req.user.use24Hour
    });
});

router.get('/version', auth.ensureAuthenticatedAPI, (req, res) => {
    res.send(VERSION);
});

router.get('/task', auth.ensureAuthenticatedAPI, (req, res) => {
   Task.find({username: req.user.username})
        .then(tasks => {
            let tasksList = [...tasks];
            //console.log(tasksList);
            Group.find({username: req.user.username})
                .then(groups => {
                    let data = [];
                    groups.forEach(group => {
                        let groupData = {
                            _id: group._id,
                            groupName: group.groupName,
                            tasks: []
                        };
                        
                        group.tasks.forEach(t => {
                            const index = tasksList.findIndex(task => task._id == t);
                            if(index === -1){
                                console.log("Error: Index when getting tasks is -1, this should never happen");
                            }
                            else{
                                const toPush = tasksList.splice(index, 1)[0];
                                groupData.tasks.push(toPush);
                            }
                        });

                        data.push(groupData);
                    });

                    if(tasksList.length > 0){
                        data.unshift({
                            _id: null,
                            groupName: null,
                            tasks: tasksList
                        });
                    }
                    res.send(data);
                })
                .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
});

router.post('/task', auth.ensureAuthenticatedAPI, (req, res) => {
    const newTask = new Task({
        username: req.user.username,
        task: req.body.task
    });

    newTask.save()
        .then(task => {
            res.send(true);
        })
        .catch(err => console.log(err));
});

router.put('/task/:id', auth.ensureAuthenticatedAPI, (req, res) => {
    Task.findOne({_id: req.params.id, username: req.user.username})
        .then(task => {
            task.task = req.body.task;
            task.save()
                .then(() => {
                    res.send(true);
                })
                .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
});

router.delete('/task/:id', auth.ensureAuthenticatedAPI, (req, res) => {
    Task.findOneAndDelete({_id: req.params.id, username: req.user.username})
        .then(task => {
            removeTaskFromGroups(req.user.username, req.params.id, () => {});
            if(task) {
                res.send(true);
            }
            else{
                res.send('err');
            }
        })
        .catch(err => console.log(err));
});

router.get('/plan', auth.ensureAuthenticatedAPI, (req, res) => {
    const date = parseInt(req.query.year+("00"+req.query.month).substr(-2)+("00"+req.query.day).substr(-2), 10);
    
    PlannedTask.find({username: req.user.username})
        .then(plan => {
            let sortedPlan = [];
            plan.forEach(p => {
                const startDate = parseInt(p.startDate.year+("00"+p.startDate.month).substr(-2)+("00"+p.startDate.day).substr(-2), 10);
                const endDate = parseInt(p.endDate.year+("00"+p.endDate.month).substr(-2)+("00"+p.endDate.day).substr(-2), 10);
                if(startDate <= date && date <= endDate){
                    sortedPlan.push(p);
                }
            });
            res.send(sortedPlan);
        })
        .catch(err => console.log(err));
});

router.put('/plan/:id', auth.ensureAuthenticatedAPI, (req, res) => {
    const {startDay, startMonth, startYear, endDay, endMonth, endYear, startHour, startMinute, endHour, endMinute} = req.body;

    const startDate = {
        day: parseInt(startDay, 10),
        month: parseInt(startMonth, 10),
        year: parseInt(startYear, 10)
    }

    const endDate = {
        day: parseInt(endDay, 10),
        month: parseInt(endMonth, 10),
        year: parseInt(endYear, 10)
    }

    const time = {
        startTime: {
            h: parseInt(startHour, 10),
            m: parseInt(startMinute, 10)
        },
        endTime: {
            h: parseInt(endHour, 10),
            m: parseInt(endMinute, 10)
        }
    }

    Task.findOneAndDelete({username: req.user.username, _id: req.params.id})
        .then(task => {
            if(task){
                removeTaskFromGroups(req.user.username, req.params.id, () => {});
                const newPlan = new PlannedTask({
                    username: task.username,
                    task: task.task,
                    time: time,
                    startDate,
                    endDate
                });

                newPlan.save()
                    .then(plan => {
                        res.send(true);
                    })
                    .catch(err => console.log(err));
            }
            else{
                res.send('err');
            }
        })
        .catch(err => console.log(err));
});

router.put('/unplan/:id', auth.ensureAuthenticatedAPI, (req, res) => {
    PlannedTask.findOneAndDelete({username: req.user.username, _id: req.params.id})
        .then(plan => {
            if(plan){
                const newTask = new Task({
                    username: plan.username,
                    task: plan.task
                });

                newTask.save()
                    .then(task => {
                        res.send(true);
                    })
                    .catch(err => console.log(err));
            }
            else{
                res.send('err');
            }
        })
        .catch(err => console.log(err));
});

router.put('/replan/:id', auth.ensureAuthenticatedAPI, (req, res) => {
    const {startHour, startMinute, endHour, endMinute, startDay, startMonth, startYear, endDay, endMonth, endYear} = req.body;

    const time = {
        startTime: {
            h: parseInt(startHour, 10),
            m: parseInt(startMinute, 10)
        },
        endTime: {
            h: parseInt(endHour, 10),
            m: parseInt(endMinute, 10)
        }
    }

    const startDate = {
        day: parseInt(startDay, 10),
        month: parseInt(startMonth, 10),
        year: parseInt(startYear, 10)
    };

    const endDate = {
        day: parseInt(endDay, 10),
        month: parseInt(endMonth, 10),
        year: parseInt(endYear, 10)
    };

    PlannedTask.findOne({username: req.user.username, _id: req.params.id})
        .then(plan => {
            if(plan){
                plan.time = time;
                plan.startDate = startDate;
                plan.endDate = endDate;

                plan.save()
                    .then(plan => {
                        res.send(true);
                    })
                    .catch(err => console.log(err));
            }
            else{
                res.send('err');
            }
        })
        .catch(err => console.log(err));
});

router.get('/done', auth.ensureAuthenticatedAPI, (req, res) => {
    const date = {
        day: parseInt(req.query.day, 10),
        month: parseInt(req.query.month, 10),
        year: parseInt(req.query.year, 10)
    }
    
    CompletedTask.find({username: req.user.username, date: date})
        .then(completed => res.send(completed))
        .catch(err => console.log(err));
});

router.put('/done/:id', auth.ensureAuthenticatedAPI, (req, res) => {
    const {day, month, year} = req.body;

    const date = {
        day: parseInt(day, 10),
        month: parseInt(month, 10),
        year: parseInt(year, 10)
    }

    Task.findOneAndDelete({username: req.user.username, _id: req.params.id})
        .then(task => {
            if(task){
                removeTaskFromGroups(req.user.username, req.params.id, () => {});
                const newCompleted = new CompletedTask({
                    username: task.username,
                    task: task.task,
                    date: date
                });

                newCompleted.save()
                    .then(completed => {
                        res.send(true);
                    })
                    .catch(err => console.log(err));
            }
            else{
                res.send('err');
            }
        })
        .catch(err => console.log(err));
});

router.put('/undone/:id', auth.ensureAuthenticatedAPI, (req, res) => {
    CompletedTask.findOneAndDelete({username: req.user.username, _id: req.params.id})
        .then(complete => {
            if(complete){
                const newTask = new Task({
                    username: complete.username,
                    task: complete.task,
                });

                newTask.save()
                    .then(task => {
                        res.send(true);
                    })
                    .catch(err => console.log(err));
            }
            else{
                res.send('err');
            }
        })
        .catch(err => console.log(err));
});

router.get('/calendar', auth.ensureAuthenticatedAPI, (req, res) => {
    const month = parseInt(req.query.month, 10);
    const year = parseInt(req.query.year, 10);

    PlannedTask.find({username: req.user.username})
        .then(tasks => {
            const filtered = tasks.filter(t => t.startDate.month === month && t.startDate.year === year);
            let content = [];

            filtered.forEach(task => {
                if(content.filter(t => t.day === task.startDate.day).length > 0){
                    content.find(t => t.day === task.startDate.day).tasks.push(task);
                }
                else{
                    content.push({
                        day: task.startDate.day,
                        tasks: [
                            task
                        ]
                    });
                }
            });

            res.send(content);
        })
        .catch(err => console.log(err));
});

//Add Task and Add to Group
router.post('/taskngroup/:groupID', auth.ensureAuthenticatedAPI, (req, res) => {
    const newTask = new Task({
        username: req.user.username,
        task: req.body.task
    });

    Group.findOne({username: req.user.username, _id: req.params.groupID})
        .then(group => {
            newTask.save()
                .then(task => {
                    group.tasks.push(task._id.toString());
                    group.save()
                        .then(() => {
                            res.send(true);
                        })
                        .catch(err => console.log(err));
                })
                .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
});

//list of groups
router.get('/groups', auth.ensureAuthenticatedAPI, (req, res) => {
    Group.find({username: req.user.username})
        .then(groups => {
            res.send(groups);
        })
        .catch(err => console.log(err));
});

//add group (pass body groupName)
router.post('/groups', auth.ensureAuthenticatedAPI, (req, res) => {
    const newGroup = new Group({
        username: req.user.username,
        groupName: req.body.groupName
    });

    newGroup.save()
        .then(group => {
            res.send(true);
        })
        .catch(err => console.log(err));
});

//delete group (pass param id)
router.delete('/groups/:id', auth.ensureAuthenticatedAPI, (req, res) => {
    Group.findOneAndDelete({username: req.user.username, _id: req.params.id})
        .then(group => {
            res.send(true);
        })
        .catch(err => console.log(err));
});

//change group name (pass param id & body groupName)
router.put('/groups/:id', auth.ensureAuthenticatedAPI, (req, res) => {
    Group.findOne({username: req.user.username, _id: req.params.id})
        .then(group => {
            group.groupName = req.body.groupName;
            group.save()
                .then(g => {
                    res.send(true);
                })
                .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
});

//Add task to group (pass param groupID & param taskID)
router.put('/group/:groupID/:taskID', auth.ensureAuthenticatedAPI, (req, res) => {

    removeTaskFromGroups(req.user.username, req.params.taskID, () => {});
    
    Group.findOne({username: req.user.username, _id: req.params.groupID})
        .then(group => {
            group.tasks.push(req.params.taskID);
            group.save()
                .then(() => {
                    res.send(true);
                })
                .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
});

//Remove task from group (pass param taskID)
router.put('/ungroup/:taskID', auth.ensureAuthenticatedAPI, (req, res) => {
    removeTaskFromGroups(req.user.username, req.params.taskID, (e) => res.send(e));
});

module.exports = router;