const express = require('express');
const router = express.Router();
const passport = require('passport');
const bcrypt = require('bcryptjs');

const auth = require('../config/auth');
const VERSION = require('../config/version');

const User = require('../models/User');
const Task = require('../models/Task');
const PlannedTask = require('../models/PlannedTask');
const CompletedTask = require('../models/CompletedTask');
const Group = require('../models/Group');

/*
router.get('/update', (req, res) => {
    //DB update commands go here
    PlannedTask.updateMany({}, [{"$set": {"endDate": "$startDate"}}], { multi: true })
        .then(() => {
            res.send("Updated");
        })
        .catch(err => res.send(err));
});
*/

router.get('/login', (req, res) => {
    const r = typeof req.query.r != 'undefined' ? '/'+decodeURIComponent(req.query.r) : '/';
    res.render('login', {
        r
    });
});

router.post('/login', (req, res, next) => {
    passport.authenticate('local', {
        successRedirect: req.body.redirect,
        failureRedirect: '/login',
        failureFlash: true
    })(req, res, next);
});

router.get('/logout', (req, res) => {
    req.logout();
    req.flash('success_msg', 'You have successfully been logged out!');
    res.redirect('/login');
});

router.get('/account', auth.ensureAuthenticated, (req, res) => {
    res.render('account', {
        user: req.user,
        version: VERSION
    });
});

router.post('/account/password', auth.ensureAuthenticated, (req, res) => {
    const {password, npassword, npassword2} = req.body;
    let errors = [];

    if(!password || !npassword || !npassword2){
        errors.push({msg: 'Please fill all fields!'});
    }

    if(npassword !== npassword2){
        errors.push({msg: 'Passwords do not match!'});
    }

    if(npassword.length < 6){
        errors.push({msg: 'Password must be at least 6 characters long!'});
    }

    User.findOne( {username: req.user.username} )
        .then(user => {
            bcrypt.compare(password, user.password, (err, isMatch) => {
                if(err) throw err;

                if(!isMatch){
                    errors.push({msg: 'Password is incorrect!'});
                }
                
                if(errors.length > 0){
                    res.render('account', {
                        errors,
                        user: req.user,
                        version: VERSION
                    });
                }
                else{
                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(npassword, salt, (err, hashPass) => {
                            if(err) throw err;
                            user.password = hashPass;
                            user.save()
                                .then(user => {
                                    req.flash('success_msg', 'Account updated!');
                                    res.redirect('/account');
                                })
                                .catch(err => console.log(err));
                        });
                    });
                }
            });
        })
        .catch(err => console.log(err));
});

router.post('/account/preferences', auth.ensureAuthenticated, (req, res) => {
    const {use24Hour} = req.body;
    User.findOne( {username: req.user.username} )
        .then(user => {
            user.use24Hour = (use24Hour ? true : false);
            user.save()
                .then(() => {
                    req.flash('success_msg', 'Account updated!');
                    res.redirect('/account');
                })
                .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
});

router.get('/account/delete', auth.ensureAuthenticated, (req, res) => {
    res.render('deleteaccount', {
        user: req.user,
        version: VERSION
    });
});

router.post('/account/delete', auth.ensureAuthenticated, (req, res) => {
    const {password, confirm} = req.body;
    User.findById(req.user._id)
        .then(userCheck => {
            let errors = [];
            
            bcrypt.compare(password, userCheck.password, (err, isMatch) => {
                if(err) throw err;

                if(!isMatch){
                    errors.push({msg: 'Password is incorrect!'});
                }

                if(!confirm){
                    errors.push({msg: 'Please check the confirm box!'});
                }
    
                if(userCheck.admin){
                    errors.push({msg: 'Admin users cannot be deleted!'});
                }

                if(errors.length > 0){
                    res.render('deleteaccount', {
                        user: req.user,
                        errors,
                        version: VERSION
                    });
                }
                else{
                    User.findByIdAndDelete(req.user._id)
                        .then((user) => {
                            Task.deleteMany({username: user.username})
                                .then(result1 => {
                                    PlannedTask.deleteMany({username: user.username})
                                        .then(result2 => {
                                            CompletedTask.deleteMany({username: user.username})
                                                .then(result3 => {
                                                    Group.deleteMany({username: user.username})
                                                        .then(result4 => {
                                                            req.logout();
                                                            req.flash('success_msg', 'Your account has been deleted!');
                                                            res.redirect('/login');
                                                        })
                                                        .catch(err => console.log(err));
                                                })
                                                .catch(err => console.log(err));
                                        })
                                        .catch(err => console.log(err));
                                })
                                .catch(err => console.log(err));
                        })
                        .catch(err => console.log(err));
                }
            });
        })
        .catch(err => console.log(err));
});

module.exports = router;